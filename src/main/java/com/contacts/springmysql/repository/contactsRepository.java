package com.contacts.springmysql.repository;

import com.contacts.springmysql.model.contacts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface contactsRepository extends JpaRepository<contacts, Long> {
}
