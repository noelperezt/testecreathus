package com.contacts.springmysql.controller;

import com.contacts.springmysql.model.contacts;
import com.contacts.springmysql.repository.contactsRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/contacts"})
public class contactsController {
    private contactsRepository repository;

    contactsController(contactsRepository contactRepository) {
        this.repository = contactRepository;
    }

    @GetMapping
    public List findAll(){
        return repository.findAll();
    }

    @GetMapping(path = {"/{id}"})
    public ResponseEntity<contacts> findById(@PathVariable long id){
        return repository.findById(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public contacts create(@RequestBody contacts entity){
        return repository.save(entity);
    }

    @PutMapping(value="/{id}")
    public ResponseEntity<contacts> update(@PathVariable("id") long id, @RequestBody contacts entity){
        return repository.findById(id)
                .map(record -> {
                    record.setName(entity.getName());
                    record.setGender(entity.getGender());
                    record.setPhone(entity.getPhone());
                    record.setEmail(entity.getEmail());
                    contacts updated = repository.save(record);
                    return ResponseEntity.ok().body(updated);
                }).orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping(path ={"/{id}"})
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        return repository.findById(id)
                .map(record -> {
                    repository.deleteById(id);
                    return ResponseEntity.ok().build();
                }).orElse(ResponseEntity.notFound().build());
    }

}
